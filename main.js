document.addEventListener('DOMContentLoaded', (event) => {

    const aNodes = document.querySelectorAll("[data-model]")

    let aItems = []

    for (let i = 0; i < aNodes.length; i++) {
        const ID = aNodes[i].id
        const sError = aNodes[i].getAttribute('data-error')
        aItems.push({
            id: ID,
            error: sError,
            value: ''
        })
    }

    const oModel = new Model(aItems)

    document.getElementById('form').addEventListener('submit', (event) => {
        event.preventDefault()
        const oData = oModel.getData()
        let nCheckRequired = 0;
        let formData = {}
        console.log(oModel)

        for (let i in oData) {
            const oItem = document.getElementById(oData[i].id)
            const sProp = oItem.name
            const sValue = oItem.value
            if (oItem.hasAttribute('data-required')) {
                const sError = oItem.getAttribute('data-error')
                const oError = oItem.nextElementSibling
                oError.innerHTML = sError

                // style required fields with no value
                if (sValue === '') {
                    oError.style.display = 'inline-block'
                    oItem.style.border = '1px solid red'
                    nCheckRequired++;
                } else {
                    oError.style.display = 'none'
                    oItem.style.border = ''
                }
            }
            formData[sProp] = sValue
        }

        // get message from submitted data after succesful field check
        if (nCheckRequired === 0) {
            console.log(formData)
            let sMessage = ''
            for (let i in formData) {
                sMessage += formData[i] + '\n'
            }
            console.log(sMessage)
            swal({
                text: "Form submitted!",
                icon: 'success'
            });
        } else {
            swal({
                text: "Please fill in required fields!",
                icon: 'error'
            });

        }
        return false;
    })
});


// model class for storing data from html
function Model(data) {
    this.data = {}
    for (let i in data) {
        this.data[i] = data[i]
    }
}

Model.prototype.getData = function () {
    return this.data;
}

